# Mastering the Basics of Python
[![MIT License](https://img.shields.io/github/license/eghysut/python-dasar?color=red&label=License&logo=GNU&style=flat-square)](https://gitlab.com/eghysut/python-basic/blob/main/LICENSE)
[![Python 3.8+](https://img.shields.io/badge/python-3.8+|3.9+|3.10+-brightgreen?logo=Python&logoColor=FFF&style=flat-square)](https://www.python.org/)
[![Facebook](https://img.shields.io/badge/facebook-groups-blue?logo=Facebook&logoColor=fff&style=flat-square)](https://web.facebook.com/groups/1547113062220560/?hoisted_section_header_type=recently_seen&multi_permalinks=3261000454165137)
[![Telegram](https://img.shields.io/badge/telegram-groups-blue?logo=Telegram&logoColor=fff&style=flat-square)](https://web.telegram.org/z/#-1052242766)
[![Twitter](https://img.shields.io/badge/twitter-@RexosP-blue?logo=twitter&logoColor=fff&style=flat-square)](https://twitter.com/RexosP)

# Channel youtube python indonesia:
[![Youtube](https://img.shields.io/badge/youtube-Kelas%20Terbuka-red?logo=Youtube&logoColor=red&style=social)](https://www.youtube.com/c/kelasterbuka)
[![Youtube](https://img.shields.io/badge/youtube-Indonesia%20Belajar-red?logo=Youtube&logoColor=red&style=social)](https://www.youtube.com/c/IndonesiaBelajarKomputer)
[![Youtube](https://img.shields.io/badge/youtube-ProgrammerZamanNow-red?logo=Youtube&logoColor=red&style=social)](https://www.youtube.com/c/ProgrammerZamanNow)
[![Youtube](https://img.shields.io/badge/youtube-Sekolah%20Koding-red?logo=Youtube&logoColor=red&style=social)](https://www.youtube.com/c/SekolahKoding)

# Website tentang python:
[![Website](https://img.shields.io/badge/website-W3Schools-darkblue.svg)](https://www.w3schools.com/python/) [![Website](https://img.shields.io/badge/website-programiz-darkblue.svg)](https://www.programiz.com/python-programming) [![Website](https://img.shields.io/badge/website-geeksforgeeks-darkblue.svg)](https://www.geeksforgeeks.org/python-programming-language/) [![Website](https://img.shields.io/badge/website-jagongoding-darkblue.svg)](https://jagongoding.com/python/) 
<!-- [![Website](https://img.shields.io/badge/website-realpython-darkblue.svg)](https://realpython.com/) -->

# Deskripsi
Proyek ini adalah sebuah sumber daya yang dirancang untuk membantu para programmer Python dengan menyediakan penjelasan dan contoh kode yang mudah dipahami. Sumber daya ini sangat cocok bagi pemula, menengah, dan bahkan para ahli dalam bahasa pemrograman Python. Berbagai topik yang dibahas dijelaskan secara dasar, dan didukung dengan contoh program yang mudah dipahami sehingga pengguna dapat memahami konsep tersebut dengan mudah. Dengan demikian, proyek ini menjadi alat yang sangat berguna bagi para pengembang Python yang ingin mengembangkan keahlian mereka dalam bahasa pemrograman yang populer ini.

<!-- [![GitHub issues](https://img.shields.io/github/issues/eghysut/python-dasar?color=brightgreen&style=flat-square&logo=git&logoColor=fff)](https://github.com/eghysut/python-dasar/issues) -->
<!-- # python basic


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/eghysut/python-basic.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/eghysut/python-basic/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->
