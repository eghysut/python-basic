# mungkin ada saatnya Anda ingin menentukan tipe pada variabel. 
# Ini bisa dilakukan dengan casting. 
# python adalah bahasa berorientasi objek, dan karena itu menggunakan kelas 
# untuk mendefinisikan tipe data, termasuk tipe primitifnya.
# casting tipe data python dilakukan dengan menggunakan fungsi konstruktor:

# int()
x = int(1.0)     # float
print(type(x), x)
# Output:
# <class 'int'> 1

y = int("4")     # str
print(type(y), y)
# Output:
# <class 'int'> 4 

b1 = int(b'5')   # bytes
print(type(b1), b1)
# <class 'int'> 5

b2 = int(0b101)  # bytes
print(type(b2), b2)
# <class 'int'> 5

oc = int(0o101)  # octal
print(type(oc), oc)
# Output:
# <class 'int'> 65 

hx = int(0x101)  # hexa
print(type(hx), hx)
# Output:
# <class 'int'> 257

t = int(True)    # boolean
print(type(t), t)
# Output:
# <class 'int'> 1 

f = int(False)   # boolean
print(type(f), f)
# Output:
# <class 'int'> 0

# Jika anda ingin mengetahui tentang fungsi-bawaan type() kunjungi folder_name: "Fungsi-Bawaan/fungsi_type.py"

# float()
x = float(1)        # int
print(type(x), x)
# Output:
# <class 'float'> 1.0

y = float("4")      # str
print(type(y), y)
# Output:
# <class 'float'> 4.0

b1 = float(b'5')    # bytes
print(type(b1), b1)
# Output:
# <class 'float'> 5.0

b2 = float(0b101)   # bytes
print(type(b2), b2)
# Output:
# <class 'float'> 5.0

oc = float(0o101)   # octal
print(type(oc), oc)
# Output:
# <class 'float'> 65.0

hx = float(0x101)   # hexa
print(type(hx), hx)
# Output:
# <class 'float'> 257.0

t = float(True)     # boolean
print(type(t), t)
# Output:
# <class 'float'> 1.0

f = float(False)    # boolean
print(type(f), f)
# Output:
# <class 'float'> 0.0
# Jika anda ingin mengetahui tentang fungsi-bawaan float() kunjungi folder_name: "Fungsi-Bawaan/fungsi_float.py"

# str()
x = str(1)
print(type(x), x)
# Output:
# <class 'str'> 1

y = str(4.0)
print(type(y), y)
# Output:
# <class 'str'> 4.0

b1 = str(b'5')
print(type(b1), b1)
# Output:
# <class 'str'> b'5'

b2 = str(0b101)
print(type(b2), b2)
# Output:
# <class 'str'> 5

oc = str(0o101)
print(type(oc), oc)
# Output:
# <class 'str'> 65

hx = str(0x101)
print(type(hx), hx)
# Output:
# <class 'str'> 257

t = str(True)
print(type(t), t)
# Output:
# <class 'str'> True

f = str(False)
print(type(f), f)
# Output:
# <class 'str'> False
# Jika anda ingin mengetahui tentang fungsi-bawaan str() kunjungi folder_name: "Fungsi-Bawaan/fungsi_str.py"

# Catatan: 
# Anda tidak dapat mengonversi bilangan kompleks menjadi jenis bilangan lain.

# Jika anda ingin mengetahui tentang fungsi-bawaan int() kunjungi folder_name: "Fungsi-Bawaan/fungsi_int.py"
print(int('1010', 2))  # biner
# Output:
# 10
print(int('1010', 8))  # octal
# Output:
# 520

print(int('1010', 10)) # default
# Output:
# 1010

print(int('1010', 16)) # hexa
# Output:
# 4112
