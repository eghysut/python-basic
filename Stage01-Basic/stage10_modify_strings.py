# memodifikasi string python
# python memiliki seperangkat method/metode/fungsi bawaan yang dapat di gunakan pada string

# huruf besar
# fungsi upper() mengembalikan string dalam huruf besar
s = "Hello World!"
print(s.upper())    
# Output:
# HELLO WORLD!

# huruf kecil
# fungsi lower() mengembalikan string dalam huruf kecil
s = "HELLO WORLD!"
print(s.lower())    
# Output:
# hello world!

# menghapus spasi
# fungsi strip() menghapus karakter spasi sebelum atau setelah string
s = " <-hello world-> "
print(s.strip())    
# Output:
#<-hello world->

# mengganti string
# fungsi replace() mengganti string dengan string lain
s = "hello world"
print(s.replace('hello', 'war'))
# Output:
# war world

# Pelajari lebih lanjut tentang method/metode/fungsi-string folder_name: "Method-String"
