# unpacking/membongkar String
# Saat membuat string, kami biasanya memberikan nilai padanya, ini disebut "packing/mengemas" string.
packing_string = "hai"
# unpacking string
x, y, z = packing_string
print(x)    
# Output:
# h

print(y)    
# Output:
# a

print(z)    
# Output:
# i

print(x,y,z)
# Output:
# h a i

# Catatan: Jumlah variabel harus sesuai dengan jumlah nilai dalam string,
# jika tidak, Anda harus menggunakan tanda bintang (*) untuk mengumpulkan
# nilai yang tersisa sebagai list.

# unpacking menggunakan tanda bintang *
packing_string = "hello world"
x, y, *z = packing_string
print(x)    
# Output:
# h

print(y)    
# Output:
# e

print(z)    
# Output:
# ['l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd']

print(x,y,z)
# Output:
# h e ['l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd']

# Jika tanda bintang ditambahkan ke nama variabel lain selain yang terakhir,
# Python akan memberikan nilai ke variabel sampai jumlah nilai yang tersisa 
# sesuai dengan jumlah variabel yang tersisa.

packing_string = "hello world"
x, *y, z = packing_string
print(x)    
# Output:
# h

print(y)    
# Output:
# ['e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l']

print(z)    
# Output:
# d

print(x,y,z)
# Output:
# h ['e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l'] d

packing_string = "alice"
x, *y, z = packing_string
# print(type(y))  # <class 'list'>
print("".join(y) + z + x)   
# Output:
# licea

# jika ingin mempelajari lebih lanjut tentang method/metode/fungsi-string folder_name: "Method-String"
# jika ingin mempelajari lebih lanjut tentang Method-String join() kunjung folder_name: "Method-String/method_join.py"
# jika ingin mempelajari lebih lanjut tentang fungsi-bawaan type() kunjung folder_name: "Fungsi-Bawaan/fungsi_type.py"
