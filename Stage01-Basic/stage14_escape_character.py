# karakter pelarian python
# ntuk menyisipkan karakter yang ilegal dalam string, gunakan karakter escape.
# karakter escape adalah garis miring terbalik \ diikuti oleh karakter yang ingin Anda sisipkan.
# Contoh karakter ilegal adalah tanda kutip ganda di dalam string yang dikelilingi oleh tanda kutip ganda:

# tanda kutip ganda "
print("hello \"world\"")    
# Output:
# hello "world"

print('hello "world"')      
# Output:
# hello "world"

# tanda kutip tunggal '
print('hello \'world\'')    
# Output:
# hello 'world'

print("hello 'world'")      
# Output:
# hello 'world'

# \\ backslash
print('hello \\ world') 
# Output:
# hello \ world

print('c:\\system32\\user\\python') 
# Output:
# c:\system32\user\python

print('\\new line')
# Output:
# \new line

print("hello \nworld")
# Output:
# hello
# world

# \r carriage return
print("hello \rworld")  
# Output:
# world

# \t tab
print("hello \tworld")  
# Output:
# hello   world

# \b backspace
print("hello \bworld")  
# Output:
# helloworld

print("hello\bworld")   
# Output:
# hellworld

# \f form feed
print("hello \fworld")  
# Output:
# hello world     
# atau Output:
# hello \x0cworld

print("hello\fworld")   
# Output:
# helloworld      
# atau Output:
# hello\x0cworld

# \ooo oktal
print("\110\145\154\154\157\40\167\157\162\154\144")    
# Output:
# Hello world

# \xhh hexa
print("\x48\x65\x6c\x6f\x20\x77\x6f\x72\x6c\x64")   
# Output:
# Hello world

# jika ingin mempelajari lebih lanjut tentang encoding kunjungi folder_name:"python-encoding"
