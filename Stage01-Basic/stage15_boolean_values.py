# python nilai boolean
# Dalam pemrograman, Anda sering perlu mengetahui apakah ekspresi itu True/Benar atau False/Salah.
# Anda dapat mengevaluasi ekspresi apapun dengan Python, dan mendapatkan salah satu dari dua jawaban,
# True/Benar atau False/Salah.
# Saat Anda membandingkan dua nilai, ekspresi dievaluasi dan Python mengembalikan nilai Boolean:

# menggunakan operator perbandingan
print(3 > 2)    
# Output:
# True

print(3 == 2)   
# Output:
# False

print(3 < 2)    
# Output:
# False

# menggunakan pernyataan if mengembalikan nilai boolean True atau False
x = 3
y = 2

if x > y:
    # jika pernyataan if bernilai boolean True
    # jalankan block di bawah ini
    print("passed")
# Output:
# passed

# menggunakan fungsi bool()
# fungsi bool() memungkinkan Anda untuk mengevaluasi nilai apa pun, dan memberi Anda True atau False sebagai nilainya
x = "hello world"
print(bool(x))  
# Output:
# True

y = 10
print(bool(y))  
# Output:
# True

print(bool("")) 
# Output:
# False

print(bool(0))  
# Output:
# False

# contoh menggunakan fungsi buatan sendiri dapat mengembalikan nilai boolean True atau False
def fungsi_true():
    return True
print(fungsi_true())    
# Output:
# True

def fungsi_false():
    return False
print(fungsi_false())   
# Output:
# False

# Python juga memiliki banyak fungsi bawaan yang mengembalikan nilai boolean,
# seperti fungsi isinstance(), yang dapat digunakan untuk menentukan apakah 
# suatu objek bertipe data tertentu
x = 10
print(isinstance(x, int))   
# Output:
# True

y = "hello world"
print(isinstance(y, str))   
# Output:
# True

# jika ingin mempelajari lebih lanjut tentang fungsi-bawaan bool() kunjungi folder_name: "Fungsi-Bawaan/fungsi_bool.py"
# jika ingin mempelajari lebih lanjut tentang fungsi-bawaan isinstance() kunjungi folder_name: "Fungsi-Bawaan/fungsi_isinstance.py"
