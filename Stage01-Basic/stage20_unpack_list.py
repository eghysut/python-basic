# unpacking/membongkar List
# Saat membuat list, kami biasanya memberikan nilai padanya, ini disebut "packing/mengemas" list.
packing_list = ['alice', 'carl', 'eliot']
# unpacking list
x, y, z = packing_list
print(x)    
# Output:
# alice 

print(y)    
# Output:
# carl

print(z)    
# Output:
# eliot

print(x, y, z)
# Output:
# alice carl eliot

# Catatan: Jumlah variabel harus sesuai dengan jumlah nilai dalam list,
# jika tidak, Anda harus menggunakan tanda bintang (*) untuk mengumpulkan
# nilai yang tersisa sebagai list.

# unpacking menggunakan tanda bintang *
packing_list = ['alice', 'carl', 'eliot', 'geral']
x, y, *z = packing_list
print(x)    
# Output:
# alice

print(y)    
# Output:
# carl

print(z)    
# Output:
# ['eliot', 'geral']

# Jika tanda bintang ditambahkan ke nama variabel lain selain yang terakhir,
# Python akan memberikan nilai ke variabel sampai jumlah nilai yang tersisa 
# sesuai dengan jumlah variabel yang tersisa.

packing_list = ['alice', 'carl', 'eliot', 'geral']
x, *y, z = packing_list
print(x)    
# Output:
# alice

print(y)    
# Output:
# ['carl', 'eliot']

print(z)    
# Output:
# geral

*x, y, z = packing_list
print(x)    
# Output:
# ['alice', 'carl']

print(y)    
# Output:
# eliot

print(z)    
# Output:
# geral
