# unpacking/membongkar Dictionary 
# Saat membuat dict, kami biasanya memberikan keys/kunci dan value/nilai padanya, ini disebut "packing/mengemas" dictionary.
packing_dict = {'nama':'alice', 'usia':23, 'alamat':'jakarta'}
# unpack dict
x, y, z = packing_dict
# akan mengembalikan/menampilkan nama keys/kunci dict
print(x, y, z)  # nama usia alamat
# mengembalikan key dan value
print(packing_dict[x])  
# Output:
# alice

print(packing_dict[y])  
# Output:
# 23

print(packing_dict[z])  
# Output:
# jakarta

# Catatan: Jumlah variabel harus sesuai dengan jumlah item dalam dictionary,
# jika tidak, Anda harus menggunakan tanda bintang (*) untuk mengumpulkan
# nilai yang tersisa sebagai tipe data list.

# unpcaking menggunakan tanda bintang *
packing_dict = {'nama':'carl', 'usia':23, 'alamat':'jakarta', 'email':'carl@gmail.com', 'jurusan':'teknik komputer'}
x, y, *z = packing_dict
print(x)    
# Output:
# nama

print(y)    
# Output:
# usia

print(z)    
# Output:
# ['alamat', 'email', 'jurusan']

print(x, y, z)  
# Output:
# nama usia ['alamat', 'email', 'jurusan']

# Jika tanda bintang ditambahkan ke nama variabel lain selain yang terakhir,
# Python akan memberikan nilai ke variabel sampai jumlah nilai yang tersisa 
# sesuai dengan jumlah variabel yang tersisa.

# mengembalikan keys/kunci
x, *y, z = packing_dict
print(x)    
# Output:
# nama

print(y)    
# Output:
# ['usia', 'alamat', 'email']

print(z)    
# Output:
# jurusan

print(x, y, z)  
# Output:
# nama ['usia', 'alamat', 'email'] jurusan

*x, y, z = packing_dict
print(x)    
# Output:
# ['nama', 'usia', 'alamat']

print(y)    
# Output:
# email

print(z)    
# Output:
# jurusan

print(x, y, z)  
# Output:
# ['nama', 'usia', 'alamat'] email jurusan

# mengembalikan value/nilai dengan "Method-Dict" values()
x, y, *z = packing_dict.values()
print(x)    
# Output:
# carl

print(y)    
# Output:
# 23

print(z)    
# Output:
# ['jakarta', 'carl@gmail.com', 'teknik komputer']

print(x, y, z)  
# Output:
# carl 23 ['jakarta', 'carl@gmail.com', 'teknik komputer']

# https://peps.python.org/pep-0448/
# unpacking menggunakan operator tanda bintang dua kali **
dict1 = {'nama':'carl', 'usia':23, 'alamat':'jakarta'}
dict2 = {'email':'carl@gmail.com', 'jurusan':'teknik komputer'}
x, y, *z = {**dict1, **dict2}   # menggabungkan dict1 dan dict2 lalu unpack
print(x)    
# Output:
# nama

print(y)    
# Output:
# usia

print(z)
# Output:
# ['alamat', 'email', 'jurusan']
